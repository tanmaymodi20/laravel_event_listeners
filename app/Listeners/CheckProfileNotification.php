<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\CheckProfileMail;
use App\Jobs\SendCheckProfileMailJob;
use App\Events\CheckProfile;



class CheckProfileNotification
{
   
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\CheckProfile  $event
     * @return void
     */
    public function handle(CheckProfile $event)
    {
        // direct send mail without using jobs
        // $users = User::get();
        // foreach ($users as $user)
        //  {
        //     Mail::to($user->email)->send(new CheckProfileMail($event->user));            
        //  }    

        $delay = now()->addSeconds(3);
        SendCheckProfileMailJob::dispatch($event->user->email)->delay($delay);
            

    }
}
