<?php

use Illuminate\Support\Facades\Route;
use App\Models\User;
use App\Events\CheckProfile;
use App\Http\Controllers\UserController;
use App\Jobs\SendCheckProfileMailJob;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () 
{
    dispatch(new SendCheckProfileMailJob())->delay(now()->addSeconds(5));
    return 'mail sent';

// event(new CheckProfile($user));      //1st method 
        // CheckProfile::dispatch($user);  // 2nd method 
});


// Route::get('/',[UserController::class,'index']);



